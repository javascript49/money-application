<?php
class ajax{
    /*Private var's*/
    private  $url;
    private  $vars;
    private  $follow;

    /*Setters*/
    public function setUrl($_value){
        $this->url = $_value;
    }
    public function setVars($_value){
        $this->vars = $_value;
    }
    public function setFollow($_value){
        $this->follow = $_value;
    }

    /*Getters*/
    public function getUrl(){
        return $this->url;
    }
    public function getVars(){
        return $this->vars;
    }
    public function getFollow(){
        return $this->follow;
    }
    
    /* general functions */
    public function createCurl(){
        $response = array();
        $ch  = curl_init($this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);  
            curl_setopt($ch, CURLOPT_HEADER,false);
            //curl_setopt($ch, CURLOPT_FOLLOWLOCATION,isset($_REQUEST['follow'])?true:false);
            curl_setopt($ch, CURLOPT_ENCODING,"");          
            curl_setopt($ch, CURLOPT_USERAGENT,"spider123");    
            curl_setopt($ch, CURLOPT_AUTOREFERER,true);       
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10);       
            curl_setopt($ch, CURLOPT_TIMEOUT,10);        
            curl_setopt($ch, CURLOPT_MAXREDIRS,10);        
            curl_setopt($ch, CURLOPT_POST,1);
            if (isset($this->vars)){
                curl_setopt($ch, CURLOPT_POSTFIELDS,$this->vars);
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);         
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);        
            curl_setopt($ch, CURLOPT_VERBOSE,1);                
            curl_setopt($ch, CURLOPT_COOKIESESSION, true);
            if(isset($this->follow)){
                $response['content'] = $this->curl_exec_follow($ch);
            }
            else{
                $response['content'] = curl_exec($ch);
            }
                $response['err']     = curl_errno($ch);
                $response['errmsg']  = curl_error($ch) ;
                $response['header'] = curl_getinfo($ch);
            curl_close($ch);
            
            return $response;
    }
    
    private function preparePostFields($array) {
        $params = array();
      
        foreach ($array as $key => $value) {
            $params[] = $key . '=' . urlencode($value);
        }
      
        return implode('&', $params);
    }
    
    private function curl_exec_follow($ch, &$maxredirect = null) {
    
        $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5)".
                      " Gecko/20041107 Firefox/1.0";
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent );
      
        $mr = $maxredirect === null ? 5 : intval($maxredirect);
      
        if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) {
      
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0);
          curl_setopt($ch, CURLOPT_MAXREDIRS, $mr);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  
        }
        else {
      
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        
            if ($mr > 0)
            {
                $original_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
                $newurl = $original_url;
                
                $rch = curl_copy_handle($ch);
                
                curl_setopt($rch, CURLOPT_HEADER, true);
                curl_setopt($rch, CURLOPT_NOBODY, true);
                curl_setopt($rch, CURLOPT_FORBID_REUSE, false);
                do
                {
                    curl_setopt($rch, CURLOPT_URL, $newurl);
                    $header = curl_exec($rch);
                    if (curl_errno($rch)) {
                        $code = 0;
                    }
                    else {
                        $code = curl_getinfo($rch, CURLINFO_HTTP_CODE);
                        if ($code == 301 || $code == 302) {
                            preg_match('/Location:(.*?)\n/', $header, $matches);
                            $newurl = trim(array_pop($matches));
                            if(!preg_match("/^https?:/i", $newurl)){
                                $newurl = $original_url . $newurl;
                            }   
                        }
                        else {
                            $code = 0;
                        }
                    }
                }
                while ($code && --$mr);
              
                curl_close($rch);
              
                if (!$mr)
                {
                    if ($maxredirect === null)
                        trigger_error('Too many redirects.', E_USER_WARNING);
                    else
                        $maxredirect = 0;
                    
                    return false;
                }
                curl_setopt($ch, CURLOPT_URL, $newurl);
            }
        }
        return curl_exec($ch);
    }
}

$ajax = new ajax;
$ajax->setUrl($_REQUEST['url']);
$ajax->setVars($_REQUEST['vars']);
$ajax->setFollow($_REQUEST['follow']);
$serverResponse = $ajax->createCurl();
echo (($serverResponse['content']));
?>