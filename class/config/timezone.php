<?php
class timeZone{
    private $timeZoneVar;
    
    public function setTimeZone ($value){
        $this->timeZoneVar = $value;
        return $this->changeTimeZone();
    }
    
    public function getTimeZone () {
        return date_default_timezone_get();
    }
    
    private function changeTimeZone() {
        try{
            date_default_timezone_set ( $this->timeZoneVar );
            return 'success';
        }catch(Exception $e){
            return $e;
        }
    }
}
?>