<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html dir="ltr">
<head>
    <title>Installation</title>
    
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=HEBREW">
    <meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=windows-1255" />
    <META HTTP-EQUIV="Content-language" CONTENT="he">
    <link rel="stylesheet" href="../../style/style.css">
</head>
    <?php
    require_once("../multiLang/lang.php");
    $lang = new lang;
    $lang->setLang('eng');
    $language = $lang->createMultiLang();
    require_once('../dataBase/dataBaseHandle.php');
    require_once("../form/inputBox.php");
    require_once("../form/selectBox.php");
    require_once("../form/form.php");
    require_once("../handlers/setForms.php");
            
    if(isset($_REQUEST['dataBaseUser'])){
    $string =
    ' <?php
        $db->setDbHost("'.$_REQUEST['dataBaseHost'].'");
        $db->setDbName("'.$_REQUEST['dataBaseName'].'"); 
        $db->setDbUser("'.$_REQUEST['dataBaseUser'].'");   
        $db->setDbPass("'.$_REQUEST['dataBasePass'].'"); 
    ?>';
                $fileLocation = "config.php";
                if (is_writable($fileLocation))
                    file_put_contents($fileLocation,$string);
                else
                    echo 'File Config.php is not writable :(\nCheck the file premissions.';
                
    $db = new dataBaseHandle(); // First Create a new DataBase connection handle object
                $db->setDbHost($_REQUEST['dataBaseHost']);// Define the DataBase Host, Usually localhost/127.0.0.1
                $db->setDbName($_REQUEST['dataBaseName']);     // Define the DataBase Name
                $db->setDbUser($_REQUEST['dataBaseUser']);     // Define the DataBase UserName
                $db->setDbPass($_REQUEST['dataBasePass']);         // Define the DataBase Password
                $db->createDataBase();     //create the new database
                $db->dbConnect();           //connect to dataBase
                $db->createTables();       //create the new tables
                $db->createAdmin($_REQUEST);       //create the Admin User
    }
    else{
        $formArray = new setForms();
        $formArray->setLanguage($language);
        
        $form = new form;
                $form->setForm($formArray->getInstallForm());
                $form->setTableClass('installForm');
                $form->setMethod('POST');
                $form->setTitle('Install Form');
                echo $form->createForm();
                
    }
    ?>
</body>
</html>