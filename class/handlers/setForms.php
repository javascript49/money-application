<?php
class setForms {
    protected $language;
    protected $requestHandler;
    
    public function setLanguage($value){
        $this->language = $value;
    }
    
    public function setRequestHandler($value){
        $this->requestHandler = $value;
    }
    
    public function getInsertInComeForm(){
        return Array(
            0 => Array(
                'type'  => 'number',
                'name' => 'amount',
                'id'   => 'amount',
                'step' => 'any',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => 'amount'
            ),
            1 => Array(
                'type'  => 'text',
                'name' => 'workplace',
                'id'   => 'workplace',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'label' => 'workplace'
            ),
            2 => Array(
                'type'  => 'month',
                'name' => 'month',
                'id'   => 'month',
                'required' => 'required',
                'label' => 'month'
            ),
            3 => Array(
                'type' => 'file',
                'name' => 'uploadFile',
                'id'   => 'uploadFile',
                'label' => 'PayCheck'
            ),
            5 => Array(
                'type'  => 'submit',
                'name' => 'subInsertInCome',
                'id'   => 'subInsertInCome',
                'value' => 'Insert InCome'
            ),
            6 => Array(
                'type'  => 'hidden',
                'name' => 'operation',
                'id'   => 'operation',
                'value' => 'insertInCome'
            ),
            //7 => Array(
            //    'type'  => 'hidden',
            //    'name' => 'token',
            //    'id'   => 'token',
            //    'value' => isset($_SESSION['userInfo']['token'])?$_SESSION['userInfo']['token']:''
            //),
            //8 => Array(
            //    'type'  => 'hidden',
            //    'name' => 'userName',
            //    'id'   => 'userName',
            //    'value' => isset($_SESSION['userInfo']['userName'])?$_SESSION['userInfo']['userName']:''
            //),
            7 => Array(
                'type'  => 'hidden',
                'name' => 'imgPath',
                'id'   => 'imgPath',
                'value' => ''
            )
        );
    }
    
    public function getInsertPayMentForm(){
        return Array(
            0 => Array(
                'type'  => 'select',
                'options' => Array('food'=>'food','transport'=>'transport','shopping'=>'shopping','traveling'=>'traveling','housing'=>'housing','goingOut'=>'goingOut',
                                   'rent'=>'rent','loanReturn'=>'loanReturn','fines'=>'fines','education'=>'education','investments'=>'investments','health'=>'health','gambling'=>'gambling','other'=>'other'
                ),
                'name' => 'type',
                'id'   => 'type',
                'required' => 'required',
                'label' => 'type'
            ),
            1 => Array(
                'type'  => 'number',
                'name' => 'amount',
                'id'   => 'amount',
                'step' => 'any',
                'required' => 'required',
                'label' => 'amount'
            ),
            2 => Array(
                'type'  => 'select',
                'options' => Array('creditCard'=>'creditCard','cash'=>'cash','payPal'=>'payPal','check'=>'check','bankTransfer'=>'bankTransfer'),
                'name' => 'payment',
                'id'   => 'payment',
                'required' => 'required',
                'label' => 'payment'
            ),
            3 => Array(
                'type'  => 'text',
                'name' => 'businessName',
                'id'   => 'businessName',
                'required' => 'required',
                'label' => 'businessName'
            ),
            4 => Array(
                'type'  => 'textarea',
                'name' => 'description',
                'id'   => 'description',
                'required' => 'required',
                'label' => 'description'
            ),
            5 => Array(
                'type'  => 'hidden',
                'name' => 'lon',
                'id'   => 'lon',
                'step' => 'any'
            ),
            6 => Array(
                'type'  => 'hidden',
                'name' => 'lat',
                'id'   => 'lat',
                'step' => 'any'
            ),
            7  =>Array(
                'type'  => 'text',
                'name' => 'addressName',
                'id'   => 'addressName',
                'required' => 'required',
                'label' => 'address'
            ),
            8 => Array(
                'type' => 'file',
                'name' => 'uploadFile',
                'id'   => 'uploadFile',
                'label' => 'Receipt'
            ),
            9 => Array(
                'type'  => 'submit',
                'name' => 'subInsertPayMent',
                'id'   => 'subInsertPayMent',
                'value' => 'Insert PayMent'
            ),
            10 => Array(
                'type'  => 'hidden',
                'name' => 'operation',
                'id'   => 'operation',
                'value' => 'insertPayMent'
            ),
            //11 => Array(
            //    'type'  => 'hidden',
            //    'name' => 'token',
            //    'id'   => 'token',
            //    'value' => isset($_SESSION['userInfo']['token'])?$_SESSION['userInfo']['token']:''
            //),
            //12 => Array(
            //    'type'  => 'hidden',
            //    'name' => 'userName',
            //    'id'   => 'userName',
            //    'value' => isset($_SESSION['userInfo']['userName'])?$_SESSION['userInfo']['userName']:''
            //),
            11 => Array(
                'type'  => 'hidden',
                'name' => 'imgPath',
                'id'   => 'imgPath',
                'value' => ''
            )
        );
    }
    
    public function getInsertNewUserForm(){
        return Array(
            0 => Array(
                'type'  => 'text',
                'name' => 'firstName',
                'id'   => 'firstName',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'label' => 'firstName',
                'value' => isset($_SESSION['userInfo']['firstName'])?$_SESSION['userInfo']['firstName']:''
            ),
            1 => Array(
                'type'  => 'text',
                'name' => 'lastName',
                'id'   => 'lastName',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'label' => 'lastName',
                'value' => isset($_SESSION['userInfo']['lastName'])?$_SESSION['userInfo']['lastName']:''
            ),
            2 => Array(
                'type'  => 'phone',
                'label' => 'phone',
                'select' => Array (
                    'type'  => 'select',
                    'options' => Array(
                        '050' => '050',
                        '052' => '052',
                        '053' => '053',
                        '054' => '054',
                        '055' => '055',
                        '057' => '057',
                        '058' => '058',
                        '05522' => '05522',
                        '05544' => '05544',
                        '05566' => '05566',
                        '05588' => '05588'
                    ),
                    'name' => 'phonePre',
                    'id'   => 'phonePre',
                    'value' => isset($_SESSION['userInfo']['phonePre'])?$_SESSION['userInfo']['phonePre']:''
                ),
                'input' => Array(
                    'name' => 'phonePost',
                    'id'   => 'phonePost',
                    'required' => 'required',
                    'type' => 'number',
                    'value' => isset($_SESSION['userInfo']['phonePost'])?$_SESSION['userInfo']['phonePost']:''
                ) 
            ),
            3 => Array(
                'type'  => 'email',
                'pattern' => '[^@]+@[^@]+\.[a-zA-Z]{2,6}',
                'name' => 'eMail',
                'id'   => 'eMail',
                'required' => 'required',
                'label' => 'eMail',
                'value' => isset($_SESSION['userInfo']['eMail'])?$_SESSION['userInfo']['eMail']:''
            ),
            4 => Array(
                'type'  => 'text',
                'name' => 'userName',
                'id'   => 'username',
                'required' => 'required',
                'label' => 'username',
                'value' => isset($_SESSION['userInfo']['userName'])?$_SESSION['userInfo']['userName']:''
            ),
            5 => Array(
                'type'  => 'password',
                'name' => 'passWord',
                'id'   => 'passWord',
                'required' => 'required',
                'label' => 'password'
            ),
            6 => Array(
                'type'  => 'select',
                'options' => Array(
                    'male' => 'male',
                    'female' => 'female'
                ),
                'name' => 'gender',
                'id'   => 'gender',
                'required' => 'required',
                'label' => 'gender',
                'value' => isset($_SESSION['userInfo']['gender'])?$_SESSION['userInfo']['gender']:''
            ),
            7 => Array(
                'type' => 'file',
                'name' => 'uploadFile',
                'id'   => 'uploadFile',
                'label' => 'Picture'
            ),
            8 => Array(
                'type'  => 'submit',
                'name' => 'subInsertNewUser',
                'id'   => 'subInsertNewUser',
                'value' => 'Insert New User'
            ),
            9 => Array(
                'type'  => 'hidden',
                'name' => 'operation',
                'id'   => 'operation',
                'value' => 'insertNewUser'
            ),
            10 => Array(
                'type'  => 'hidden',
                'name' => 'imgPath',
                'id'   => 'imgPath',
                'value' => ''
            )
        );
    }
    
    public function getUpdateInComeForm(){
        return Array(
            0 => Array(
                'type'  => 'month',
                'name' => 'month',
                'id'   => 'month',
                'required' => 'required',
                'label' => 'select Month'
            ),
            1 => Array(
                'type'  => 'number',
                'name' => 'amount',
                'id'   => 'amount',
                'step' => 'any',
                'required' => 'required',
                'label' => 'amount'
            ),
            2 => Array(
                'type'  => 'text',
                'name' => 'workplace',
                'id'   => 'workplace',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'label' => 'workplace'
            ),
            3  =>Array(
                'type'  => 'button',
                'name' => 'viewImg',
                'id'   => 'viewImg',
                'value' => 'View Image'
            ),
            4 => Array(
                'type'  => 'submit',
                'name' => 'subInsertInCome',
                'id'   => 'subInsertInCome',
                'value' => 'Update InCome'
            ),
            5 => Array(
                'type'  => 'hidden',
                'name' => 'operation',
                'id'   => 'operation',
                'value' => 'updateInCome'
            )
            //6 => Array(
            //    'type'  => 'hidden',
            //    'name' => 'token',
            //    'id'   => 'token',
            //    'value' => isset($_SESSION['userInfo']['token'])?$_SESSION['userInfo']['token']:''
            //),
            //7 => Array(
            //    'type'  => 'hidden',
            //    'name' => 'userName',
            //    'id'   => 'userName',
            //    'value' => isset($_SESSION['userInfo']['userName'])?$_SESSION['userInfo']['userName']:''
            //),
        );
    }
    
    public function getUpdatePayMentForm(){
        return Array(
            0 => Array(
                'type'  => 'date',
                'name' => 'date',
                'id'   => 'date',
                'required' => 'required',
                'label' => 'select Date'
            ),
            1 => Array(
                'type'  => 'select',
                'options' => Array('',''),
                'name' => 'paymentChoice',
                'id'   => 'paymentChoice',
                'required' => 'required',
                'label' => 'select PayMent'
            ),
            2 => Array(
                'type'  => 'select',
                'options' => Array('food'=>'food','transport'=>'transport','shopping'=>'shopping','traveling'=>'traveling','housing'=>'housing','goingOut'=>'goingOut',
                                   'rent'=>'rent','loanReturn'=>'loanReturn','fines'=>'fines','education'=>'education','investments'=>'investments','health'=>'health','gambling'=>'gambling','other'=>'other'
                ),
                'name' => 'type',
                'id'   => 'type',
                'required' => 'required',
                'label' => 'type'
            ),
            3 => Array(
                'type'  => 'number',
                'name' => 'amount',
                'id'   => 'amount',
                'step' => 'any',
                'required' => 'required',
                'label' => 'amount'
            ),
            4 => Array(
                'type'  => 'select',
                'options' => Array('creditCard'=>'creditCard','cash'=>'cash','payPal'=>'payPal','check'=>'check','bankTransfer'=>'bankTransfer'),
                'name' => 'payment',
                'id'   => 'payment',
                'required' => 'required',
                'label' => 'payment'
            ),
            5 => Array(
                'type'  => 'text',
                'name' => 'businessName',
                'id'   => 'businessName',
                'required' => 'required',
                'label' => 'businessName'
            ),
            6 => Array(
                'type'  => 'textarea',
                'name' => 'description',
                'id'   => 'description',
                'required' => 'required',
                'label' => 'description'
            ),
            7 => Array(
                'type'  => 'hidden',
                'name' => 'lon',
                'id'   => 'lon',
                'step' => 'any',
            ),
            8 => Array(
                'type'  => 'hidden',
                'name' => 'lat',
                'id'   => 'lat',
                'step' => 'any',
            ),
            9  =>Array(
                'type'  => 'text',
                'name' => 'addressName',
                'id'   => 'addressName',
                'required' => 'required',
                'label' => 'address'
            ),
            10 => Array(
                'type' => 'file',
                'name' => 'uploadFile',
                'id'   => 'uploadFile',
                'label' => 'Receipt'
            ),
            11  =>Array(
                'type'  => 'button',
                'name' => 'viewImg',
                'id'   => 'viewImg',
                'value' => 'View Image'
            ),
            12 => Array(
                'type'  => 'submit',
                'name' => 'subInsertPayMent',
                'id'   => 'subInsertPayMent',
                'value' => 'Update PayMent'
            ),
            13 => Array(
                'type'  => 'hidden',
                'name' => 'operation',
                'id'   => 'operation',
                'value' => 'updatePayMent'
            ),
            //14 => Array(
            //    'type'  => 'hidden',
            //    'name' => 'token',
            //    'id'   => 'token',
            //    'value' => isset($_SESSION['userInfo']['token'])?$_SESSION['userInfo']['token']:''
            //),
            //15 => Array(
            //    'type'  => 'hidden',
            //    'name' => 'userName',
            //    'id'   => 'userName',
            //    'value' => isset($_SESSION['userInfo']['userName'])?$_SESSION['userInfo']['userName']:''
            //),
            14 => Array(
                'type'  => 'hidden',
                'name' => 'currentDate',
                'id'   => 'currentDate',
                'disabled' => 'disabled',
                'value' => date("Y-m-d")
            ),
            15 => Array(
                'type'  => 'hidden',
                'name' => 'imgPath',
                'id'   => 'imgPath',
                'value' => ''
            )
        );
    }
    
    public function getLoginForm(){
        return Array(
            0 => Array(
                'type'  => 'text',
                'name' => 'userName',
                'id'   => 'username',
                'required' => 'required',
                'label' => 'username'
            ),
            1 => Array(
                'type'  => 'password',
                'name' => 'passWord',
                'id'   => 'passWord',
                'required' => 'required',
                'label' => 'password'
            ),
            2 => Array(
                'type'  => 'submit',
                'name' => 'subLogin',
                'id'   => 'subLogin',
                'value' => 'Login'
            ),
            3 => Array(
                'type'  => 'hidden',
                'name' => 'operation',
                'id'   => 'operation',
                'value' => 'login'
            )
        );
    }
    
    public function getMenuForm(){
        return Array(
            0 => Array(
                'type'  => 'button',
                'name' => 'insertPayMentMenu',
                'class' => 'menuItem',
                'id'   => 'insertPayMentMenu',
                'value' => 'Insert PayMent'
            ),
            1 => Array(
                'type'  => 'button',
                'name' => 'insertInComeMenu',
                'class' => 'menuItem',
                'id'   => 'insertInComeMenu',
                'value' => 'Insert InCome'
            ),
            2 => Array(
                'type'  => 'button',
                'name' => 'updateUserMenu',
                'class' => 'menuItem',
                'id'   => 'updateUserMenu',
                'value' => 'Update User'
            ),
            3 => Array(
                'type'  => 'button',
                'name' => 'updatePayMentMenu',
                'class' => 'menuItem',
                'id'   => 'updatePayMentMenu',
                'value' => 'Update PayMent'
            ),
            4 => Array(
                'type'  => 'button',
                'name' => 'updateInComeMenu',
                'class' => 'menuItem',
                'id'   => 'updateInComeMenu',
                'value' => 'Update InCome'
            ),
            5 => Array(
                'type'  => 'button',
                'name' => 'exitMenu',
                'class' => 'menuItem',
                'id'   => 'exitMenu',
                'value' => 'Exit'
            ),
        );
    }
    
    public function getMenuForm2(){
        return Array(
            0 => Array(
                'type'  => 'button',
                'name' => 'byType',
                'class' => 'menuItem',
                'id'   => 'byType',
                'value' => 'By Type'
            ),
            1 => Array(
                'type'  => 'button',
                'name' => 'byLocation',
                'class' => 'menuItem',
                'id'   => 'byLocation',
                'value' => 'By Location'
            ),
            2 => Array(
                'type'  => 'button',
                'name' => 'byPayment',
                'class' => 'menuItem',
                'id'   => 'byPayment',
                'value' => 'By Payment'
            ),
            3 => Array(
                'type'  => 'button',
                'name' => 'inVsOut',
                'class' => 'menuItem',
                'id'   => 'inVsOut',
                'value' => 'In Vs Out'
            ),
            4 => Array(
                'type'  => 'button',
                'name' => 'byComboChart',
                'class' => 'menuItem',
                'id'   => 'byComboChart',
                'value' => 'By Combo Chart'
            ),
            5 => Array(
                'type'  => 'button',
                'name' => 'settings',
                'class' => 'menuItem',
                'id'   => 'settings',
                'value' => 'Settings'
            ),
        );
    }
    
    public function getSummerProtoType(){
        return Array(
            0 => Array(
                'type'  => 'select',
                'options' => Array('day'=>'Day View','month'=>'Month View','year'=>'Year View'),
                'name' => 'dateType',
                'id'   => 'dateType',
                'required' => 'required',
                'label' => 'Format'
            ),
            1 => Array(
                'type'  => 'date',
                'name' => 'datePicker',
                'id'   => 'datePicker',
                'required' => 'required',
                'label' => 'date'
            ),
            2 => Array(
                'type'  => 'hidden',
                'name' => 'bySum',
                'id'   => 'bySum',
                'value'=> 'none'
            ),
            3 => Array(
                'type'  => 'hidden',
                'name' => 'value',
                'id'   => 'value'
            ),
            4 => Array(
                'type'  => 'hidden',
                'name' => 'operation',
                'id'   => 'operation',
                'value' => 'selectPayMentByDateAndType'
            )
        );
    }
    
    public function getSummerByTypeForm(){
        $form = $this->getSummerProtoType();
        $form[2]['value'] = 'type';
        return $form;
    }
    
    public function getSummerByLocationForm(){
        $form = $this->getSummerProtoType();
        $form[2]['value'] = 'location';
        return $form;
    }
    
    public function getSummerByPaymentForm(){
        $form = $this->getSummerProtoType();
        $form[2]['value'] = 'payment';
        return $form;
    }
    
    public function getSummerByComboChartForm(){
        $form = $this->getSummerProtoType();
        $form[2]['value'] = 'none';
        return $form;
    }
}
?>