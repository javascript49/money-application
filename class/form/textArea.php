<?php
class textArea extends inputBox{
    /*Private var's*/
    protected  $rows;
    protected  $cols;
    protected  $maxLength;
    
    
    /*Setters*/
    public function setRows($_value){
        $this->rows = $_value;
    }
    public function setCols($_value){
        $this->cols = $_value;
    }
    public function setMaxLength($_value){
        $this->maxLength = $_value;
    }
    
    /*Getters*/
    public function getRows(){
        return $this->rows;
    }
    public function getCols(){
        return $this->cols;
    }
    public function getMaxLength(){
        return $this->maxLength;
    }
    
    /* general functions */
    public function createInput(){
        
            
        if (isset($this->label))
            $textArea = '<label>'.$this->label.'</label> <textarea ';
        else    
            $textArea = '<textarea ';
        
        if (isset($this->maxLength))
            $textArea.= ' maxlength = "'.$this->maxLength.'" ';
        if (isset($this->Class))
            $textArea.= ' class = "'.$this->Class.'" ';
        if (isset($this->readOnly))
            $textArea.= ' readonly = "'.$this->readOnly.'" ';
        if (isset($this->pattern))
            $textArea.= ' pattern = "'.$this->pattern.'" ';
        if (isset($this->required))
            $textArea.= ' required = "'.$this->required.'" ';
        if (isset($this->rows))
            $textArea.= ' rows = "'.$this->rows.'" ';
        if (isset($this->cols))
            $textArea.= ' cols = "'.$this->cols.'" ';
        if (isset($this->name))
            $textArea.= ' name = "'.$this->name.'" ';
        if (isset($this->id))
            $textArea.= ' id = "'.$this->id.'" ';
        if (isset($this->placeHolder))
            $textArea.= ' placeHolder = "'.$this->placeHolder.'" ';
        if (isset($this->disabled))
            $textArea.= ' disabled = "'.$this->disabled.'" ';
       
        $textArea.= ' > ';
        if (isset($this->value))
            $textArea.= $this->value;
            
        $textArea.= '</textarea>';
        
        return $textArea;
    }
}

?>