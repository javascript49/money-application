<?php
require('dataBaseHandler.php');
require('queryHandler.php');

class requestHandler extends queryHandler{
    
    private $dbObj;
    private $installer = false;
    protected $timeStamp;
    
    public function __construct(){
        
        date_default_timezone_set ( 'Asia/Tel_Aviv' );
        $this->timeStamp = date('Y-m-d G:i:s');
        
        $db = new dataBaseHandle();
            $db->setDbHost("184.168.44.85");
            $db->setDbName("moneySaveApp"); 
            $db->setDbUser("moneySaveApp");   
            $db->setDbPass("DorCohen666#");
            $db->setTimeZone("+3:00");
            $this->dbObj = $db->dbConnect();
        //parent::__construct();
        $this->creaeteCall();
            
    }
    public function creaeteCall(){
        parent::setDbObj($this->dbObj);
        if (isset($_REQUEST['operation'])){
            parent::setDataArray($_REQUEST);
            if ( $_REQUEST['operation'] == 'login' ){
                $userData = parent::selectLogin('');
                   
                if( ($userData['requestStatus'] == 'success' ) && isset($userData['data'][0]['id']) ){
                    $data = array();
                    $data['id'] = $userData['data'][0]['id'];
                    
                    $key = "dor".mt_rand(1000000, 9999999)."cohen";//uniqid()."dorCohen";
                    $data['token'] = parent::encrypt($key,'public');
                    parent::updateToken($data);
                    parent::getQuery();
                    $userData = parent::selectLogin('');
                    
                    /* remove this line after testing */
                    $userData['enKey'] = parent::encrypt($key,'private');
                    $userData['deKey'] = parent::decrypt($data['token'],'public');
                    /* remove this line after testing */
                    print_r(parent::jsonResponse($userData));
                    exit();
                }
                else{
                    print_r('failure');
                    exit();
                }
            }
            else if ( $_REQUEST['operation'] == 'apiResponse' ){
                    
                print_r( parent::apiResponse() );
                exit();
                    
            }
            else if ( $_REQUEST['operation'] == 'installDataBase' ){
                if ($this->installer == true){
                    parent::createDcIncome();
                    parent::createDcOutCome();
                    parent::createDcUsers();
                    parent::createConstraints();
                    echo "Installing";
                    exit();
                }
                else{
                    echo "Fuck Off";
                    exit();
                }
            }
            else if ( $_REQUEST['operation'] == 'updateHash' ){
                if ($this->installer == true){
                    parent::updateHash(null);
                    echo "Updating Hash Keys";
                    exit();
                }
                else{
                    echo "Fuck Off";
                    exit();
                }
            }
            else if ( isset($_REQUEST['operation']) ){
                $responseString['ipAddress'] = $_SERVER['REMOTE_ADDR'];
                $responseString['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                $allowedOperations =    Array(
                                            0   =>  Array(
                                                        'operation'=>'selectPayMentByDate',
                                                        'parameter'=>array('dateStart','uid')
                                                    ),
                                            1   =>  Array(
                                                        'operation'=>'selectPayMentByDateAndParameter',
                                                        'parameter'=>array('parameter','parameterCont','uid','dateStart','dateEnd',)
                                                    ),
                                            2   =>  Array(
                                                        'operation'=>'selectPayMentByDateAndAmount',
                                                        'parameter'=>array('dateStart','dateEnd','amountStart','amountEnd','uid')
                                                    ),
                                            3   =>  Array(
                                                        'operation'=>'selectInComeByDate',
                                                        'parameter'=>array('dateStart','uid')
                                                    ),
                                            4   =>  Array(
                                                        'operation'=>'insertPayMent',
                                                        'parameter'=>array('type','uid','amount','payment','lon','lat','businessName','addressName','imgPath')
                                                    ),
                                            5   =>  Array(
                                                        'operation'=>'insertInCome',
                                                        'parameter'=>array('amount','uid','workplace','month','imgPath')
                                                    ),
                                            6   =>  Array(
                                                        'operation'=>'insertNewUser',
                                                        'parameter'=>array('firstName','lastName','phonePre','phonePost','userName','passWord','eMail','gender','imgPath')
                                                    ),
                                            7   =>  Array(
                                                        'operation'=>'updatePayMent',
                                                        'parameter'=>array('type','uid','amount','payment','lon','lat','businessName','addressName','imgPath','paymentChoice')
                                                    ),
                                            8   =>  Array(
                                                        'operation'=>'updateInCome',
                                                        'parameter'=>array('amount','uid','workplace','month','imgPath','id')
                                                    ),
                                            9   =>  Array(
                                                        'operation'=>'updateUser',
                                                        'parameter'=>array('firstName','lastName','phonePre','phonePost','userName','passWord','eMail','gender','imgPath','uid')
                                                    ),
                                            10  =>  Array(
                                                        'operation'=>'selectPayMentBetweenDates',
                                                        'parameter'=>array('dateStart','uid','dateEnd')
                                                    ),
                                            11  =>  Array(
                                                        'operation'=>'autoComplete',
                                                        'parameter'=>array('field','uid','keyword')
                                                    ),
                                            12  =>  Array(
                                                        'operation'=>'selectPayMentByDateAndType',
                                                        'parameter'=>array('dateType','value','uid','bySum')
                                                    ),
                                            13  =>  Array(
                                                        'operation'=>'getNearMe',
                                                        'parameter'=>array('limit','uid','lon','lat','radius')
                                                    )
                                        );
                $validOperation = false;

                foreach ($allowedOperations as $operation){
                    if ($operation['operation'] == $_REQUEST['operation']){
                        if ((count($_REQUEST)) - (count($operation['parameter'])+3) > 2 ){
                            $validOperation=false;
                            break;
                        }
                        foreach ($operation['parameter'] as $parameter){
                            if ((!isset($_REQUEST[$parameter]))&&($parameter!='uid')){
                                $validOperation=false;
                                break;
                            }
                            else
                                $validOperation=true;
                        }
                    }
                }
                if ($validOperation == false) {
                    $responseString['serverResponse'] = 'Illegal Operation, Access Denied ';
                    print_r(parent::jsonResponse($responseString));
                    exit();
                }
                $userData = parent::selectUserByUserName($_REQUEST['userName']);
                $userData = $userData['data'];
                $_REQUEST['token']=isset($_REQUEST['token'])?$_REQUEST['token']:'null';
                $firstDecryption = parent::decrypt($_REQUEST['token'],'private');
                $token = parent::encrypt($firstDecryption,'public');
                
                $certificatStart = substr($firstDecryption, 0, 3);
                $certificatEnd   = substr($firstDecryption, strlen($firstDecryption)-5, 5);
                if (($token != $userData[0]['token'])||($certificatStart != "dor")||($certificatEnd != "cohen")){
                    $responseString['serverResponse'] = 'Illegal Token, Access Denied ';
                    print_r(parent::jsonResponse($responseString));
                    exit();
                }
                else{
                    $_REQUEST['uid'] = $userData[0]['id'];
                    $_REQUEST['requestStatus'] = 'success';
                    parent::setDataArray($_REQUEST);
                    
                    eval("\$response = parent::".$_REQUEST['operation']."('');");
                    if ($response == null){
                        $response['ipAddress'] = $_SERVER['REMOTE_ADDR'];
                        $response['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                        $response['requestStatus'] = 'failure';
                        $response['serverResponse'] = 'Missing Parameter';
                    }
                    print_r(parent::jsonResponse($response));
                    exit();
                }
            }
        }
        else{
            $responseString['ipAddress'] = $_SERVER['REMOTE_ADDR'];
            $responseString['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
            $responseString['serverResponse'] = 'Illegal Operation, Access Denied ';
            print_r(parent::jsonResponse($responseString));
        }
    }
}
$requestHandler = new requestHandler;
?>