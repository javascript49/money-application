<?php
class queryHandler{
    
    /*  -------->   Private Var's   <--------*/
    private $dbObj;
    private $query;
    private $dataArray = array();
    private $privkey = 'DorCohenServer123456';
    private $pubkey  = 'DorCohenApplicationCode123456';
    protected $timeStamp;
    
    /*  -------->   Setters Function <--------*/
    protected function setDataArray($value){
        
        $this->dataArray = $value;
    }
    protected function setDbObj($value){
        $this->dbObj = $value;
    }
    protected function setQuery($value){
        $this->query = $value;
    }
    
    /*  -------->   Getters Function <--------*/
    protected function getDataArray(){
        return $this->dataArray ;
    }
    protected function getDbObj(){
        return $this->dbObj ;
    }
    protected function getQuery(){
        return $this->query ;
    }
    
    /*  -------->   Construct Function   <--------*/
    function __construct(){
    }
     
    /*  -------->   Select Functions   <--------*/
    protected function selectPayMentByDate($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
        
        $startDate = date('Y-m-d H:i:s',strtotime($data['dateStart']));
        
        $this->query = "SELECT * FROM `dcoutcome` WHERE YEAR(`date`) = YEAR('".$startDate."') AND MONTH(`date`) = MONTH('".$startDate."') AND DAY(`date`) = DAY('".$startDate."') AND `uid` = '".$data['uid']."' ";
            
        return $this->Execute('');
    }
    
    protected function autoComplete($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        $startDate = date('Y-m-d H:i:s',strtotime($data['dateStart']));
        
        $this->query = "SELECT `".$data['field']."` FROM `dcoutcome` WHERE (`".$data['field']."` REGEXP '^ ".$data['keyword']."' OR `".$data['field']."` REGEXP '^".$data['keyword']."') AND `uid` = '".$data['uid']."' LIMIT 5";
            
        return $this->Execute('');
    }
    
    
    protected function getNearMe($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        $this->query = "SELECT `lat`,`lon`,`businessName`,`addressName`,`type`,`latLonNameIndex` FROM `dcoutcome` WHERE `uid` = '".$data['uid']."' GROUP BY `latLonNameIndex` ";
        $results = $this->Execute('');
        
        foreach ($results['data'] as $key=>&$result){
            if (isset($result['lat'])&&isset($result['lon'])&&isset($data['lat'])&&isset($data['lon'])&&($result['lat']!='0')&&($result['lon']!='0')&&(strlen($result['businessName'])>1)){
                $this->query = "SELECT `amount`,`lat`,`lon`,`description` AS `title` FROM `dcoutcome` WHERE `uid` = '".$data['uid']."' AND `latLonNameIndex` = '".$result['latLonNameIndex']."' GROUP BY `description`,`businessName` ";
                $descriptions = $this->ExecuteInner('');
                $lonAvg = 0;
                $latAvg = 0;
                $resultCounter = 0;
                foreach ($descriptions as $key2=>&$description){
                    $lonAvg += $description['lon'];
                    $latAvg += $description['lat'];
                    $resultCounter++;
                }
                $result['lon'] = (string)($lonAvg/$resultCounter);
                $result['lat'] = (string)($latAvg/$resultCounter);
                $result['description'] = $descriptions;
                
                $R = 6371;
                $dLat = deg2rad((float)$data['lat'])-deg2rad((float)$result['lat']);
                $dLon = deg2rad((float)$data['lon'])-deg2rad((float)$result['lon']);
                
                $a    =  (sin($dLat/2) * sin($dLat/2)) + (cos(deg2rad((float)$result['lat'])) * cos(deg2rad((float)$data['lat'])) *  sin($dLon/2) * sin($dLon/2)); 
                $c = 2 * atan2(sqrt($a), sqrt(1-$a)); 
                $d = number_format((float)($R * $c), 3, '.', '');;
                $result['distance'] = (string)$d;
                if ($data['radius']<$d){
                    unset($results['data'][$key]);
                }
                
            }
            else{
                unset($results['data'][$key]);
            }
        }
        
        $sorter=array();
        $ret=array();
        reset($results['data']);
        foreach ($results['data'] as $ii => $va) {
            $sorter[$ii]=$va['distance'];
        }
        asort($sorter);
        $rstKey = 0;
        foreach ($sorter as $ii => $va) {
            $ret[$rstKey]=$results['data'][$ii];
            $rstKey++;
            if ($rstKey>=$data['limit'])
                break;
        }
        $results['data']=$ret;

        return $results;
    }
    
    protected function selectPayMentBetweenDates($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        $endDate = strtotime($data['dateEnd']);
        $startDate = strtotime($data['dateStart']);
        if ($startDate>$endDate){
            $temp = $startDate;
            $startDate = $endDate;
            $endDate = $temp;
        }
        $endDate = date('Y-m-d H:i:s',$endDate);
        $startDate = date('Y-m-d H:i:s',$startDate);
        
        $this->query = "SELECT * FROM `dcoutcome` WHERE `date` BETWEEN '".$startDate."' AND '".$endDate."' AND `uid` = '".$data['uid']."' ";

        return $this->Execute('');
    }
    
    protected function selectPayMentByDateAndParameter($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        $this->query = "SELECT * FROM `dcoutcome`   WHERE ".$data['parameter']." = '".$data['parameterCont']."'
                                                    AND `date` BETWEEN '".$data['dateStart']."' AND '".$data['dateEnd']."'
                                                    AND `uid` = '".$data['uid']."' ";
        return $this->Execute('');
    }
    
    protected function selectPayMentByDateAndAmount($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
                
        $this->query = "SELECT * FROM `dcoutcome`   WHERE `date` BETWEEN '".$data['dateStart']."' AND '".$data['dateEnd']."'
                                                    AND `amount` BETWEEN '".$data['amountStart']."' AND '".$data['amountEnd']."'
                                                    AND `uid` = '".$data['uid']."' ";
        return $this->Execute('');
    }
    
    protected function selectPayMentByDateAndType($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        $query = ""; 
            
        if ($data['dateType']=='year'){
            $data['year'] = date('Y-m-d H:i:s',strtotime($data['value']));
            $query.= "YEAR(`date`) = YEAR('".$data['value']."') AND ";
        }
        else if ($data['dateType']=='month'){
            $data['month'] = date('Y-m-d H:i:s',strtotime($data['value']));
            $query.= "YEAR(`date`) = YEAR('".$data['value']."') AND MONTH(`date`) = MONTH('".$data['value']."') AND ";
        }
        else if ($data['dateType']=='day'){
            $data['day'] = date('Y-m-d H:i:s',strtotime($data['value']));
            $query.= "YEAR(`date`) = YEAR('".$data['value']."') AND MONTH(`date`) = MONTH('".$data['value']."') AND DAY(`date`) = DAY('".$data['value']."') AND ";
        }
        
        $query.= "`uid` = '".$data['uid']."'  "; 
        
        if ($data['bySum']=='type'){
            $secondStatment = "SELECT SUM(amount) AS `totalAmount`,count(id) AS `totalTransactions`,`type` FROM `dcoutcome` WHERE ";
            $this->query = $secondStatment.$query." GROUP BY `type` ";
        }
        else if ($data['bySum']=='payment'){
            $thirdStatment  = "SELECT SUM(amount) AS `totalAmount`,count(id) AS `totalTransactions`,`payment` FROM `dcoutcome` WHERE ";
            $this->query = $thirdStatment.$query." GROUP BY `payment` ";
        }
        else if ($data['bySum']=='location'){
            $thirdStatment  = "SELECT SUM(amount) AS `totalAmount`,count(id) AS `totalTransactions`,SUBSTRING(`addressName`,INSTR(`addressName`,',')+1) AS `locations` FROM `dcoutcome` WHERE ";
            $this->query = $thirdStatment.$query." GROUP BY `locations` ";
        }
        else{
            $firstStatment  = "SELECT * FROM `dcoutcome` WHERE ";
            $this->query = $firstStatment.$query;
        }

        return $this->Execute('');
    }
    
    protected function selectUser($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
        if ((!isset($data['id']))){
            return null;
        }       
        $this->query = "SELECT * FROM `dcusers` WHERE `id` = '".$data['id']."' ";
        return $this->Execute('');
    }
    
    protected function selectUserByUserName($userName){
        
        if ((!isset($userName))){
            return null;
        }       
        $this->query = "SELECT * FROM `dcusers` WHERE `userName` = '".$userName."' ";
        return $this->Execute('');
    }
    
    protected function selectLogin($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        if ((!isset($data['userName']))||(!isset($data['passWord']))){
            return null;
        }
        
        $password = $data['passWord'];
        $this->query = "SELECT * FROM `dcusers` WHERE `userName` = '".$data['userName']."' AND `passWord` = '".$password."' ";
        return $this->Execute('');
    }
    
    protected function selectInComeByDate($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
         
        $this->query = "SELECT * FROM `dcincome` WHERE `month` = '".$data['dateStart']."-01' AND `uid` = '".$data['uid']."' ";
            
        return $this->Execute('');
    }
    
    /*  -------->   Insert Functions   <--------*/
    protected function insertPayMent($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
        
        $lonFormat = number_format($data['lon'], 1, '.', '');
        $latFormat = number_format($data['lat'], 1, '.', '');
        $hashString = md5("".$latFormat.":".$lonFormat.":".$data['businessName']."");
            
        $this->query = "INSERT INTO  `dcoutcome` (  `type` ,
                                                    `amount` ,
                                                    `payment` ,
                                                    `lon` ,
                                                    `lat` ,
                                                    `businessName`,
                                                    `addressName`,
                                                    `uid`,
                                                    `imgPath`,
                                                    `description`,
                                                    `date`,
                                                    `latLonNameIndex`
                                                                    ) VALUES (  '".$data['type']."',
                                                                                '".$data['amount']."',
                                                                                '".$data['payment']."',
                                                                                '".$data['lon']."',
                                                                                '".$data['lat']."',
                                                                                '".$data['businessName']."',
                                                                                '".$data['addressName']."',
                                                                                '".$data['uid']."',
                                                                                '".$data['imgPath']."',        
                                                                                '".$data['description']."',
                                                                                '".$this->timeStamp."',
                                                                                '".$hashString."'            );";
        return $this->Execute('');
    }
    
    protected function insertInCome($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
        
        $this->query = "INSERT INTO  `dcincome` (   `amount` ,
                                                    `workplace`,
                                                    `month`,
                                                    `uid`,
                                                    `imgPath`
                                                                ) VALUES (  '".$data['amount']."',
                                                                            '".$data['workplace']."',
                                                                            '".$data['month']."-01',
                                                                            '".$data['uid']."',
                                                                            '".$data['imgPath']."'     );";
        return $this->Execute('');
    }
    
    protected function insertNewUser($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
           
        $this->query = "INSERT INTO  `dcusers` (    `firstName` ,
                                                    `lastName` ,
                                                    `phonePre` ,
                                                    `phonePost` ,
                                                    `userName` ,
                                                    `passWord` ,
                                                    `eMail`  ,
                                                    `gender`,
                                                    `imgPath`
                                                                    ) VALUES (  '".$data['firstName']."',
                                                                                '".$data['lastName']."',
                                                                                '".$data['phonePre']."',
                                                                                '".$data['phonePost']."',
                                                                                '".$data['userName']."',
                                                                                '".$data['passWord']."',
                                                                                '".$data['eMail']."',
                                                                                '".$data['gender']."',
                                                                                '".$data['imgPath']."'     );";
    return $this->Execute('');
    }
    
    /*  -------->   Update Functions   <--------*/
    protected function updatePayMent($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        $this->query = " UPDATE  `dcoutcome` SET
                                                `type` =  '".$data['type']."',
                                                `amount` =  '".$data['amount']."',
                                                `payment` =  '".$data['payment']."',
                                                `lon` =  '".$data['lon']."',
                                                `lat` =  '".$data['lat']."',
                                                `addressName` = '".$data['addressName']."',
                                                `businessName` =  '".$data['businessName']."',
                                                `description`  =   '".$data['description']."',
                                                `imgPath`  =   '".$data['imgPath']."'
                                                                        WHERE  `id` = '".$data['paymentChoice']."' AND `uid` = '".$data['uid']."' ; ";
    return $this->Execute('');
            
    }
    
    protected function updateInCome($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        $this->query = " UPDATE  `dcincome` SET
                                                `amount` =  '".$data['amount']."',
                                                `workplace` =  '".$data['workplace']."',
                                                `month` = '".$data['month']."-01'
                                                                        WHERE  `id` = '".$data['id']."' AND `uid` = '".$data['uid']."' ; ";
    return $this->Execute('');
            
    }
    
    protected function updateUser($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
            
        $this->query = " UPDATE  `dcusers` SET
                                                `firstName` =  '".$data['firstName']."',
                                                `lastName` =  '".$data['lastName']."',
                                                `phonePre` =  '".$data['phonePre']."',
                                                `phonePost` =  '".$data['phonePost']."',
                                                `userName` =  '".$data['userName']."',
                                                `passWord` =  '".$data['passWord']."',
                                                `eMail` =  '".$data['eMail']."',
                                                `gender` =  '".$data['gender']."'
                                                                        WHERE  `id` = '".$data['uid']."' ; ";
    return $this->Execute('');
            
    }
    
    protected function updateToken($dataObj){
        
        $data = array();
        if (empty($dataObj))
            $data = $this->dataArray;
        else
            $data = $dataObj;
        
        $date = date_create();
        $timeStamp = date_format($date, 'Y-m-d H:i:s') ;
            
        $this->query = " UPDATE `dcusers` SET `token` =  '".$data['token']."' , `tokenExp`= '".$timeStamp."' WHERE  `id` = '".$data['id']."' ; ";
        return $this->Execute('');
            
    }
    
    protected function updateHash($dataObj){
        
        $this->query = "SELECT * FROM `dcoutcome` ";
            $inf = $this->Execute('');
            
            foreach ($inf['data'] as $data){
            
                    $lonFormat = number_format($data['lon'], 1, '.', '');
                    $latFormat = number_format($data['lat'], 1, '.', '');
                    $hashString = md5("".$latFormat.":".$lonFormat.":".$data['businessName']."");
                    
                        
                    $this->query = " UPDATE  `dcoutcome` SET
                                            `latLonNameIndex` = '".$hashString."'
                                                WHERE  `id` = '".$data['id']."'; ";
                    $this->ExecuteInner('');
            }
            
            
            return ;
    }
    
    
    /*  -------->   Api Function   <--------*/
    protected function apiResponse(){
            
        $op = Array(
            'selectOperation' => Array (
                'selectPayMentByDate' => Array(
                    '1' => 'dateStart',
                    '2' => 'dateEnd'
                ),
                'selectPayMentByDateAndParameter' => Array(
                    '1' => 'parameter',
                    '2' => 'parameterCont',
                    '3' => 'dateStart',
                    '4' => 'dateEnd'
                ),
                'selectPayMentByDateAndAmount' => Array(
                    '1' => 'dateStart',
                    '2' => 'dateEnd',
                    '3' => 'amountStart',
                    '4' => 'amountEnd'
                ),
                'selectInComeByDate' => Array(
                    '1' => 'dateStart',
                    '2' => 'dateEnd'
                )
            ),
            'insertOperation' => Array (
                'insertPayMent' => Array(
                    '1' => 'type',
                    '2' => 'amount',
                    '3' => 'payment',
                    '4' => 'lon',
                    '5' => 'lat',
                    '6' => 'businessName',
                    '7' => 'addressName'
                ),
                'insertInCome' => Array(
                    '1' => 'amount',
                    '2' => 'workplace',
                    '3' => 'month'
                ),
                'insertNewUser' => Array(
                    '1' => 'firstName',
                    '2' => 'lastName',
                    '3' => 'phonePre',
                    '4' => 'phonePost',
                    '5' => 'userName',
                    '6' => 'passWord',
                    '7' => 'eMail',
                    '8' => 'gender'
                )
            ),
            'updateOperation' => Array (
                'updatePayMent' => Array(
                    '1' => 'type',
                    '2' => 'amount',
                    '3' => 'payment',
                    '4' => 'lon',
                    '5' => 'lat',
                    '6' => 'businessName',
                    '7' => 'addressName',
                    '8' => 'id'
                ),
                'updateInCome' => Array(
                    '1' => 'amount',
                    '2' => 'workplace',
                    '3' => 'id',
                    '4' => 'month'
                ),
                'updateUser' => Array(
                    '1' => 'firstName',
                    '2' => 'lastName',
                    '3' => 'phonePre',
                    '4' => 'phonePost',
                    '5' => 'userName',
                    '6' => 'passWord',
                    '7' => 'eMail',
                    '8' => 'gender'
                )
            )
        );
        return json_encode( $op ); 
    }
    
    /*  -------->   Return Function   <--------*/
    protected function jsonResponse($dataObj){
        
        $data = array();
        if (empty($dataObj)){
            $data = $this->dataArray;
        }
        else
            $data = $dataObj;
            
        if (isset($data)){
            return json_encode( (array) $data ); 
        }
        
    }
    
    /*  -------->   Execute Function   <--------*/
    private function Execute($dataObj){
            
        $data = array();
        if (empty($dataObj))
            $data = $this->query;
        else
            $data = $dataObj;
        if (isset($data)){
            try {
                
                $serverResponse = $this->dbObj->prepare($data) or die("ERROR: " . implode(":", $this->dbObj->errorInfo()));
                $serverResponse->execute();
                if(!($serverResponse->rowCount())){
                    $serverRes['requestStatus'] = 'failure';
                }
                else{
                    $serverRes['data']  = $serverResponse->fetchAll(PDO::FETCH_ASSOC);
                    $serverRes['requestStatus'] = 'success';
                }
                
                $serverRes['ipAddress'] = $_SERVER['REMOTE_ADDR'];
                $serverRes['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                
            }catch(PDOException $e) {
                $serverRes = $e->getMessage();
                $serverRes['requestStatus'] = 'failure';
            }
            return $serverRes;
        }
    }
    
    private function ExecuteInner($dataObj){
            
        $data = array();
        if (empty($dataObj))
            $data = $this->query;
        else
            $data = $dataObj;
        if (isset($data)){
            try {
                
                $serverResponse = $this->dbObj->prepare($data) or die("ERROR: " . implode(":", $this->dbObj->errorInfo()));
                $serverResponse->execute();
                if(($serverResponse->rowCount())){
                    $serverRes = $serverResponse->fetchAll(PDO::FETCH_ASSOC);
                }
                
            }catch(PDOException $e) {
                $serverRes = $e->getMessage();
            }
            return $serverRes;
        }
    }
    
    /*  -------->   Encrypt Function   <--------*/
    protected function encrypt($data,$keyCup){
        
        if ($keyCup == 'public')
            $key = md5($this->pubkey);
        else 
            $key = md5($this->privkey);
                
        $plaintext_utf8 = utf8_encode($data);
        
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext_utf8, MCRYPT_MODE_ECB);

        $ciphertext_base64 = (string)bin2hex($ciphertext);
                
        return  trim($ciphertext_base64) ;
        
    }
    
    /*  -------->   Decrypt Function   <--------*/
    protected function decrypt($data,$keyCup){
        
        if ($keyCup == 'public')
            $key = md5($this->pubkey);
        else 
            $key = md5($this->privkey);
        
        $ciphertext_base64 = $this->hex2bin($data);
        
        $ciphertext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_base64, MCRYPT_MODE_ECB);
        
        $plaintext_utf8 = (string)utf8_decode($ciphertext);
        
        return  trim($plaintext_utf8) ;
    
    }
    
    /*  -------->   Install Function   <--------*/
    protected function createDcIncome(){
        $this->query = "
        CREATE TABLE IF NOT EXISTS `dcincome` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `month` date NOT NULL,
            `amount` float NOT NULL DEFAULT '0',
            `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `workplace` varchar(256) NOT NULL DEFAULT 'None',
            `uid` int(11) NOT NULL,
            PRIMARY KEY (`id`),
            KEY `uid` (`uid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ";
        return $this->Execute('');
    }
    
    protected function createDcOutCome(){
        $this->query = "
        CREATE TABLE IF NOT EXISTS `dcoutcome` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `type` enum('food','transport','shopping','traveling','housing','goingOut','rent','loanReturn','fines','education','investments','health','gambling','other') NOT NULL DEFAULT 'other',
            `amount` float NOT NULL DEFAULT '0',
            `payment` enum('creditCard','cash','payPal','check','bankTransfer') NOT NULL DEFAULT 'cash',
            `lon` float NOT NULL DEFAULT '0',
            `lat` float NOT NULL DEFAULT '0',
            `businessName` varchar(512) NOT NULL DEFAULT 'None',
            `uid` int(11) NOT NULL,
            `description` text NOT NULL,
            `addressName` varchar(1028) NOT NULL,
            PRIMARY KEY (`id`),
            KEY `uid` (`uid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ";
        return $this->Execute('');
    }
    
    protected function createDcUsers(){
        $this->query = "
        CREATE TABLE IF NOT EXISTS `dcusers` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `firstName` varchar(128) NOT NULL DEFAULT 'None',
            `lastName` varchar(128) NOT NULL DEFAULT 'None',
            `phonePre` int(8) NOT NULL DEFAULT '0',
            `phonePost` int(16) NOT NULL DEFAULT '0',
            `userName` varchar(128) NOT NULL DEFAULT 'None',
            `passWord` varchar(128) NOT NULL DEFAULT '123456',
            `token` varchar(512) NOT NULL DEFAULT 'None',
            `tokenExp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `eMail` varchar(512) NOT NULL DEFAULT 'None',
            `gender` enum('male','female') NOT NULL DEFAULT 'male',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ";
        return $this->Execute('');
    }
    
    protected function createConstraints(){
        $this->query = "
        ALTER TABLE `dcincome`
            ADD CONSTRAINT `dcincome_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `dcusers` (`id`) ON UPDATE NO ACTION;
        ALTER TABLE `dcoutcome`
            ADD CONSTRAINT `dcoutcome_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `dcusers` (`id`) ON UPDATE NO ACTION;
        ";
        return $this->Execute('');
    }
    
    /*  -------->   Utils Functions   <--------*/
    private function hex2bin($h)
    {
        if ((!is_string($h))||(strlen($h)%2!=0))
            return null;
        $r='';
        for ($a=0; $a<strlen($h); $a+=2) {
                $r.=chr(hexdec($h{$a}.$h{($a+1)}));
        }
        return $r;
    }
    
}
?>