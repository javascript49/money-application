var Location = {
    watchMode : false,
    watchInt : false,
    watchTry : 0,
    testSupport:function(){
        if (navigator.geolocation){
            return "Location : Supported";   
        }
        else{
            return "Location : Unsupported";
        }
    },
    getLocation : function (locationSuccess,locationFailure) {
        navigator.geolocation.getCurrentPosition(
            function(position){
                var currentLocation = new Array();
                currentLocation['latitude']=position.coords.latitude;
                currentLocation['longitude']=position.coords.longitude;
                currentLocation['accuracy']=position.coords.accuracy;
                currentLocation['altitude']=position.coords.altitude;
                currentLocation['heading']=position.coords.heading;
                currentLocation['timestamp']=position.coords.timestamp;
                locationSuccess(currentLocation);
                return currentLocation;
            },
            function(error)
            {
                switch(error.code) 
                {
                    case error.PERMISSION_DENIED:
                        errorMsg = "User denied the request for Geolocation."
                        break;
                    case error.POSITION_UNAVAILABLE:
                        errorMsg = "Location information is unavailable."
                        break;
                    case error.TIMEOUT:
                        errorMsg = "The request to get user location timed out."
                        break;
                    case error.UNKNOWN_ERROR:
                        errorMsg = "An unknown error occurred."
                        break;
                }
                locationFailure(errorMsg);
                return errorMsg;
            },
            { enableHighAccuracy: true, timeout: 10000, maximumAge: 0 }
        )
    }
}