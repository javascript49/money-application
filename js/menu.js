var menu = {
    getAction : function (){
        $('html').on("click",'#insertPayMentMenu',function() {
            service.chagnePage('insertPayMent',pageFunction.insertPayMent);
        });
        
        $('html').on("click",'#insertInComeMenu',function() {
            service.chagnePage('insertInCome',pageFunction.insertInCome);
        });
        
        $('html').on("click",'#updatePayMentMenu',function() {
            service.chagnePage('updatePayMent',pageFunction.updatePayMent);
        });
        
        $('html').on("click",'#updateInComeMenu',function() {
            service.chagnePage('updateInCome',pageFunction.updateInCome);
        });
          
        $('html').on("click",'#updateUserMenu',function() {
            service.chagnePage('updateUser',pageFunction.updateUser);
        });
        
        $('html').on("click",'#exitMenu',function() {
            service.chagnePage('exit',pageFunction.exit);
        });
        
        $('html').on("click",'#byType',function() {
            service.chagnePage('byType',pageFunction.summarize);
        });
        
        $('html').on("click",'#byLocation',function() {
            service.chagnePage('byLocation',pageFunction.summarize);
        });
        
        $('html').on("click",'#byPayment',function() {
            service.chagnePage('byPayment',pageFunction.summarize);
        });
        
        $('html').on("click",'#byComboChart',function() {
            service.chagnePage('byComboChart',pageFunction.summarize);
        });
        
        $('html').on("click",'#inVsOut',function() {
            service.chagnePage('inVsOut',pageFunction.summarize);
        });
        
        $('html').hammer().on("dragleft",'#MenuForm',function() {
            if ($('#menuHolder').css('left')=="0px")
                $('#menuHolder').animate({left: '100%'},500,"swing");
        });
        
        $('html').hammer().on("dragright",'#MenuForm',function() {
            if ($('#menuHolder').css('left')!="0px")
                $('#menuHolder').animate({left: '0%'},500,"swing");
        });
    },
    
    doOnLoad : function() {
        $('body').css('min-height',$(window).height());
        $('body').css('min-width',$(window).width());
        $('body').css('max-width',$(window).width());
        
        storage.listAll();
        
    }
}