var session = {
    setItem:function(name,data) {        
        sessionStorage.setItem(name, JSON.stringify(data));
    },
    getItem:function(name) {
        var data = sessionStorage.getItem(name);
        return jQuery.parseJSON(data);
    },
    listAll:function(){
        for (i=0; i<=sessionStorage.length-1; i++)  
        {   
            key = sessionStorage.key(i);  
            val = sessionStorage.getItem(key);
            console.log("Storage Objects  -  key : %s  value : %s",key,val);
        }    
    },
    testSupport:function(){
        if (sessionStorage)  
            return "Local Session: Supported";   
        else  
            return "Local Session: Unsupported";     
    },
    clearValue:function(name){
      void removeItem(name);  
    },
    clearAll:function(){
        sessionStorage.clear();
    }
}