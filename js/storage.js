var storage = {
    setItem:function(name,data) {        
        localStorage.setItem(name, JSON.stringify(data));
    },
    getItem:function(name) {
        var data = localStorage.getItem(name);
        return jQuery.parseJSON(data);
    },
    listAll:function(){
        for (i=0; i<=localStorage.length-1; i++)  
        {   
            key = localStorage.key(i);  
            val = localStorage.getItem(key);
            console.log("Storage Objects  -  key : %s  value : %s",key,val);
        }    
    },
    testSupport:function(){
        if (localStorage)  
            return "Local Storage: Supported";   
        else  
            return "Local Storage: Unsupported";     
    },
    clearValue:function(name){
      void removeItem(name);  
    },
    clearAll:function(){
        localStorage.clear();
    }
}