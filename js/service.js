var service = {
    
    autoCompleteLock : false,
    
    lock : false,
    
    preFix : '',
    
    loading : function(status){
        if(status == true){
            $('#loadAnim').show();
        }
        else{
            $('#loadAnim').hide();
        }
    },
    
    getLock : function(status){
        if(status == true){
            service.lock = true;
            $('#loadAnim').show();
        }
        else{
            service.lock = false;
            $('#loadAnim').hide();
        }
    },
    
    setSession : function(name,data){
        storage.setItem(name,data);
    },
    
    getSession : function(name){
        return storage.getItem(name);    
    },
    
    ajaxModel : function (url,data,done) {
        if (service.lock == false){
            service.getLock(true);
            $.ajax({
                url  : url,
                type : 'POST',
                data : data
            })
            .done(function (res) {
                service.getLock(false);
                if(toolBelt.isJson(res) == true){
                    res = JSON.parse(res);
                    if(res.serverResponse === "Illegal Token, Access Denied "){
                        service.chagnePage('exit',pageFunction.exit);
                    }
                    else{
                        done(res);
                    }
                }
                else{
                    done(res);
                }
            })
            .fail(function(){
                service.getLock(false);
            })
            .always(function () {
                service.getLock(false);
            });
        }
    },
    
    multiAjaxRequest : function(url,data,done){
        if (service.lock == false)
            service.ajaxModel(url,data,done);
        else
            setTimeout(function() { service.multiAjaxRequest(url,data,done) },'1000');
    },
   
    chagnePage : function (page,pFunction) {
        service.setSession('lastPage',page);
        service.ajaxModel(''+service.preFix+'/content/'+page+'.php','',function(data){
            $('body').off();
            $('#ajaxBody').html(data);
            pFunction();
        });
    },
    
    doLogin : function (element){
        var formData = encodeURIComponent(element.serialize());
        var dataA = encodeURIComponent("http://holonmotor.com/money/class/dataBase/requestHandler.php");
        service.ajaxModel(''+service.preFix+'/class/ajax/ajax.php','url='+dataA+'&vars='+formData+'&follow=true',function(data){
            userData = data.data[0];
            service.setSession('userData',userData);
            service.ajaxModel(
                ''+service.preFix+'/content/session.php',
                'token='+userData.token+'&userName='+userData.userName+'&phonePre='+userData.phonePre+'&phonePost='+userData.phonePost+'&lastName='+userData.lastName+'&firstName='+userData.firstName+'&eMail='+userData.eMail+'',
                function(data){
                    var uSession = service.getSession('userData');
                    uSession.token = data;
                    service.setSession('userData',uSession);
                    
                    var lastPage = storage.getItem("lastPage");
                    if ((typeof(lastPage)!='undefined')&&((lastPage)!='exit')&&((lastPage)!='login'))
                        service.chagnePage(lastPage,eval("pageFunction."+lastPage+""));
                    else
                        service.chagnePage('insertPayMent',pageFunction.insertPayMent);
            });
        });
    },
    
    doInsert : function(element){
        var uSession = service.getSession('userData');
        var formData = encodeURIComponent(element.serialize()+'&userName='+uSession.userName+'&token='+uSession.token);
        var dataA = encodeURIComponent("http://holonmotor.com/money/class/dataBase/requestHandler.php");
        service.ajaxModel(''+service.preFix+'/class/ajax/ajax.php','url='+dataA+'&vars='+formData+'&follow=true',function(data){
            if (data.requestStatus == 'success'){
                element.trigger('reset');
                Location.getLocation(processFunction.locationSuccess,processFunction.locationFailure);
            }
            alert(data.requestStatus);
        });
    },
    
    doUpdate : function(element){
        var uSession = service.getSession('userData');
        var formData = encodeURIComponent(element.serialize()+'&userName='+uSession.userName+'&token='+uSession.token);
        var dataA = encodeURIComponent("http://holonmotor.com/money/class/dataBase/requestHandler.php");
        service.ajaxModel(''+service.preFix+'/class/ajax/ajax.php','url='+dataA+'&vars='+formData+'&follow=true',function(data){
            alert(data.requestStatus);
        });
    },
    
    doUploadFile : function (myFile,element,success) {
        if (service.lock == false){
            service.getLock(true);
            var FileName = myFile.name;
            var FileSize = myFile.size;
            var FileType = myFile.type;
            
            var formData = new FormData();
            formData.append('uploadFile', myFile);
            var xhr = new XMLHttpRequest();
            xhr.open("POST", ''+service.preFix+'/content/imageUpload.php');
            xhr.send(formData);
            xhr.onreadystatechange= function() {
                if (xhr.readyState==4){
                    success(xhr);
                }
            }
        }
    },
    
    getGoogleAddress : function(currentLocation) {
        var dataA = encodeURIComponent('http://maps.googleapis.com/maps/api/geocode/json?latlng='+currentLocation['latitude']+','+currentLocation['longitude']+'&sensor=false');
        service.multiAjaxRequest(''+service.preFix+'/class/ajax/ajax.php','url='+dataA,function(data){
            $('#addressName').val(data.results[0].formatted_address);
        });
    },
    
    getNearBy : function (currentLocation) {
        
        var success = function(data) {
            $(document).on('change','#description2',function(){
                var opSelect = $(this).find("option:selected").attr('key');
                if (opSelect == 'free') {
                    $('#description2').remove();
                    $('#description').show().removeAttr('disabled');
                }
                else{
                    $('#amount').val($("#description2 option:selected").attr('amount'))
                }
            });
            $('#businessName2').on('change',function(){
                var opSelect = $(this).find("option:selected").attr('key');
                if (opSelect == 'free') {
                    $('#businessName2,#description2').remove();
                    $('#businessName,#description').show().removeAttr('disabled');
                }
                else{
                    
                    var pSelect = "<select id='description2'  name='description'>";
                    for (var key2 in data[opSelect].description) {
                        pSelect +="<option key='"+key2+"' amount='"+ data[opSelect].description[key2].amount  +"' value='"+ data[opSelect].description[key2].title +"'>"+ data[opSelect].description[key2].title +"</option>";
                    }
                    pSelect +="<option key='free'>Free Text</option>";
                    pSelect += "</select>";
                    $('#description').hide().attr('disabled','disabled');
                    $('#description2').remove();
                    $('#description').parent().append(pSelect);
                    
                    $('#type').val(data[opSelect].type);
                    $('#amount').val(data[opSelect].description[0].amount);
                }
            });
        }
        
        var userNearMe = service.getUserNearBy(currentLocation,success);
        //var userNearMe = service.getGoogleNearBy(currentLocation,success);
    },
    
    getGoogleNearBy : function (currentLocation,success) {
        
        var googleToken = 'AIzaSyAAhuE5AgHakO8zVMkP59YJtzaLK-cdYns';
        var placeType = 'amusement_park|aquarium|art_gallery|atm|bakery|bank|bar|beauty_salon|bicycle_store|book_store|bowling_alley|'
                        + 'cafe|car_dealer|car_rental|car_repair|car_wash|casino|clothing_store|convenience_store|dentist|department_store|'
                        +'electronics_store|florist|food|furniture_store|gas_station|grocery_or_supermarket|hair_care|hardware_store|'
                        +'home_goods_store|jewelry_store|liquor_store|lodging|meal_takeaway|mosque|movie_rental|movie_theater|museum|night_club|'
                        +'pet_store|pharmacy|post_office|restaurant|shoe_store|shopping_mall|spa|stadium|store|subway_station|'
                        +'train_station|veterinary_care|zoo';
        var dataB = encodeURIComponent('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+currentLocation['latitude']+','+currentLocation['longitude']+'&radius='+(parseInt(currentLocation['accuracy'])+50)+'&types='+placeType+'&sensor=false&key='+googleToken+'');
        service.multiAjaxRequest(''+service.preFix+'/class/ajax/ajax.php','url='+dataB,function(data){
            var dSelect = "<select id='businessName2' name='businessName'>";
            for (var key in data.results) {
               dSelect +="<option key='"+key+"' value='"+ data.results[key].name +"'>"+ data.results[key].name +"</option>";
            }
            var description='';
            for (var key in data.results[0].types) {
                description += data.results[0].types[key]+", ";
            }
            dSelect +="<option key='free'>Free Text</option>";
            dSelect += "</select>";
            $('#businessName').hide().attr('disabled','disabled');
            $('#businessName').parent().append(dSelect); 
            $('#description').val(description);
            
            success(data.results);
        }); 
    },
    
    getUserNearBy : function (currentLocation,success) {
        var radius = (parseInt(currentLocation['accuracy'])+50)/1000;
        var uSession = service.getSession('userData');
        var formData = encodeURIComponent('lon='+currentLocation['longitude']+'&lat='+currentLocation['latitude']+'&operation=getNearMe&limit=10&radius='+radius+'&userName='+uSession.userName+'&token='+uSession.token);
        var dataA = encodeURIComponent("http://holonmotor.com/money/class/dataBase/requestHandler.php");
        service.multiAjaxRequest(''+service.preFix+'/class/ajax/ajax.php','url='+dataA+'&vars='+formData+'&follow=true',function(data){
            data = data.data;
            if (data.length){
                var dSelect = "<select id='businessName2' name='businessName'>";
                for (var key in data) {
                    dSelect +="<option key='"+key+"' value='"+ data[key].businessName +"'>"+ data[key].businessName +"</option>";
                }
                dSelect +="<option key='free'>Free Text</option>";
                dSelect += "</select>";
                
                
                $('#businessName').hide().attr('disabled','disabled');
                $('#businessName').parent().append(dSelect);
                
                var pSelect = "<select id='description2'  name='description'>";
                for (var key2 in data[0].description) {
                    pSelect +="<option key='"+key2+"' value='"+ data[0].description[key2].title +"'>"+ data[0].description[key2].title +"</option>";
                }
                pSelect +="<option key='free'>Free Text</option>";
                pSelect += "</select>";
                $('#description').hide().attr('disabled','disabled');
                $('#description').parent().append(pSelect);
                
                $('#type').val(data[0].type);
                $('#amount').val(data[0].description[0].amount);
                
                success(data);
            }
        });
    },
    
    getInComeByDate : function (dateStart,success) {
        var uSession = service.getSession('userData');
        var formData = encodeURIComponent('dateStart='+dateStart+'&operation=selectInComeByDate&userName='+uSession.userName+'&token='+uSession.token);
        var dataA = encodeURIComponent("http://holonmotor.com/money/class/dataBase/requestHandler.php");
        service.ajaxModel(''+service.preFix+'/class/ajax/ajax.php','url='+dataA+'&vars='+formData+'&follow=true',function(data){
            data = data.data;
            success(data);
            return data;
        });
    },
    
    getPayMentsByDate : function (dateStart,dateEnd,success) {
        var uSession = service.getSession('userData');
        var formData = encodeURIComponent('dateStart='+dateStart+'&dateEnd='+dateEnd+'&operation=selectPayMentByDate&userName='+uSession.userName+'&token='+uSession.token);
        var dataA = encodeURIComponent("http://holonmotor.com/money/class/dataBase/requestHandler.php");
        service.ajaxModel(''+service.preFix+'/class/ajax/ajax.php','url='+dataA+'&vars='+formData+'&follow=true',function(data){
            data = data.data;
            success(data);
            return data;
        });
    },
    
    getPayMentsByType : function (element,dataType,dateType) {
        var uSession = service.getSession('userData');
        var formData = encodeURIComponent(element.serialize()+'&userName='+uSession.userName+'&token='+uSession.token);
        var dataA = encodeURIComponent("http://holonmotor.com/money/class/dataBase/requestHandler.php");
        service.ajaxModel(''+service.preFix+'/class/ajax/ajax.php','url='+dataA+'&vars='+formData+'&follow=true',function(data){
            charts.destroyChart();
            charts.data = data;
            charts.dateType = dateType;
            if (dataType == 'none')
                google.load("visualization", "1", {"callback" : charts.drawColumnChart,"packages":["corechart"]});
            else
                google.load("visualization", "1", {"callback" : charts.drawPieChart,"packages":["corechart"]});
        });
    },
    
    autoComplete : function (field,keyword) {
        if (service.autoCompleteLock == false){
            service.autoCompleteLock = true;
            var uSession = service.getSession('userData');
            var formData = encodeURIComponent('field='+field+'&keyword='+keyword+'&operation=autoComplete&userName='+uSession.userName+'&token='+uSession.token);
            var dataA = encodeURIComponent("http://holonmotor.com/money/class/dataBase/requestHandler.php");
            service.ajaxModel(''+service.preFix+'/class/ajax/ajax.php','url='+dataA+'&vars='+formData+'&follow=true',function(data){
                data = data.data;
                var text = eval("data[0]."+field+"");
                if (text){
                    var keySplit = keyword.split(" ");
                    var splitText = text.split(" ");
                    var completeText="";
                    for (var i=0; i<keySplit.length;i++){
                        completeText +=splitText[i]+ " ";
                    }
                    $("#"+field).val(completeText);
                    setTimeout(function(){service.autoCompleteLock = false; toolBelt.setCaretToPos(document.getElementById(field),completeText.length)},1000);
                }
                return data;
            });
        }
    },
    
}
