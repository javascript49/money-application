var googleResult;

$(document).ready(function() {
    menu.doOnLoad();
    menu.getAction();
    
    var uSession = storage.getItem('userData');
    if ((uSession!=null)){
        var lastPage = storage.getItem("lastPage");
        if ((typeof(lastPage)!='undefined')&&((lastPage)!='exit')){
            if ((lastPage == 'byType')||(lastPage == 'byLocation')||(lastPage == 'byPayment')||(lastPage == 'byComboChart'))
                service.chagnePage(lastPage,eval("pageFunction.summarize"));
            else
                service.chagnePage(lastPage,eval("pageFunction."+lastPage+""));
        }
        else{
            service.chagnePage('login',pageFunction.login);
        }
    }
    else{
        service.chagnePage('login',pageFunction.login);
    }
    
});
