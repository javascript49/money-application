var pageFunction = {
    
    login : function () {
        $('body').on("submit",'#loginForm',function(e) {
            e.preventDefault();
            service.doLogin($(this));
        });
    },
    
    insertPayMent : function (){
        Location.getLocation(processFunction.locationSuccess,processFunction.locationFailure);
        $('body').on("submit",'#insertPayMentForm',function(e) {
            e.preventDefault();
            
            var myFileList = document.getElementById('uploadFile').files;
            var myFile = myFileList[0];
            var element = $(this);
            
            if (typeof(myFile) == 'object'){
                service.doUploadFile(myFile,element, function (xhr) {
                    service.getLock(false);
                    data1=JSON.parse(xhr.responseText);
                    if (data1.server == 'File uploaded successfully'){
                        $('#imgPath').val(data1.url);
                        service.doInsert(element);
                    }
                })
            }
            else{
                $('#imgPath').val('none');
                service.doInsert(element);
            }
        });
        //$('body').on("keydown",'#description',function (e) {
        //    if ((e.keyCode != 46)&&(e.keyCode != 8)){
        //        var field = $(this).attr('name');
        //        var keyword = $(this).val();
        //        toolBelt.checkForStopTyping(function(){service.autoComplete(field,keyword)},500);
        //    }
        //});
    },
    
    insertInCome : function () {
        Location.getLocation(processFunction.locationSuccess,processFunction.locationFailure);
        $('body').on("submit",'#insertInComeForm',function(e) {
            e.preventDefault();
            
            var myFileList = document.getElementById('uploadFile').files;
            var myFile = myFileList[0];
            var element = $(this);
            
            if (typeof(myFile) == 'object'){
                service.doUploadFile(myFile,element, function (xhr) {
                    service.getLock(false);
                    data1=JSON.parse(xhr.responseText);
                    if (data1.server == 'File uploaded successfully'){
                        $('#imgPath').val(data1.url);
                        service.doInsert(element);
                    }
                })
            }
            else{
                $('#imgPath').val('none');
                service.doInsert(element);
            }
        });    
    },
    
    updatePayMent : function (){
        $('body').on("change",'#date',function () {
            var sdate = $(this).val();
            var edate = $('#currentDate').val();
            var doOnResponse =  function(response){
                storage.setItem('tempResponse',response);
                var option="";
                for (var i in response){
                    if (response[i].businessName)
                        option +="<option value='"+response[i].id+"' num='"+i+"'>"+response[i].businessName+"</option>";
                }
                $("#paymentChoice").html(option);
                $("#paymentChoice").prop("selectedIndex", -1);
                $("#date").trigger('blur');
                toolBelt.openSelect("paymentChoice");
            }
            service.getPayMentsByDate(sdate,edate,doOnResponse);
        });
        $('body').on("change",'#paymentChoice',function () {
            var tempResponse = storage.getItem('tempResponse');
            var selectedId = $("#paymentChoice option:selected").attr('num');
            $('#type').val(tempResponse[selectedId].type);
            $('#businessName').val(tempResponse[selectedId].businessName);
            $('#description').val(tempResponse[selectedId].description);
            $('#lat').val(tempResponse[selectedId].lat);
            $('#lon').val(tempResponse[selectedId].lon);
            $('#payment').val(tempResponse[selectedId].payment);
            $('#amount').val(tempResponse[selectedId].amount);
            $('#addressName').val(tempResponse[selectedId].addressName);
            $('#viewImg').attr('url',tempResponse[selectedId].imgPath)
        });
        $('body').on("click",'#viewImg',function () {
            var url = $(this).attr('url');
            window.open(url);
        });
        $('body').on("submit",'#updatePayMentForm',function(e) {
            e.preventDefault();
            
            var myFileList = document.getElementById('uploadFile').files;
            var myFile = myFileList[0];
            var element = $(this);
            
            if (typeof(myFile) == 'object'){
                service.doUploadFile(myFile,element, function (xhr) {
                    service.getLock(false);
                    data1=JSON.parse(xhr.responseText);
                    if (data1.server == 'File uploaded successfully'){
                        $('#imgPath').val(data1.url);
                        service.doUpdate(element);
                    }
                })
            }
            else{
                $('#imgPath').val('none');
                service.doUpdate(element);
            }
        });    
    },
    
    updateInCome : function () {
        $('body').on("change",'#month',function () {
            var sdate = $(this).val();
            var doOnResponse = function(response){
                $('#amount').val(response[0].amount);
                $('#workplace').val(response[0].workplace);
                $('#viewImg').attr('url',response[0].imgPath)
            }
            service.getInComeByDate(sdate,doOnResponse);
        });
        $('body').on("click",'#viewImg',function () {
            var url = $(this).attr('url');
            window.open(url);
        });
        $('body').on("submit",'#updateInComeForm',function(e) {
            e.preventDefault();
            var element = $(this);
            service.doUpdate(element);
        });
    },
    
    updateUser : function () {
        
    },
    
    exit : function () {
        storage.clearAll();
        session.clearAll();
        service.chagnePage('login',pageFunction.login);
    },
    
    summarize : function () {
        $('body').on("change",'#dateType',function(e) {
            var option = $(this).val();
            switch(option){
                case 'day':
                    $('#datePicker').attr('type','date');
                    break;
                //case 'month':
                //    $('#datePicker').attr('type','month');
                //    break;
                //case 'year':
                //    $('#datePicker').attr({'type':'number','min':'1970','max':'2030'});
                //    break;
                default:
                    $('#datePicker').attr('type','date');
                    break;
            }
        });
        $('body').on("change",'#datePicker',function(e) {
            var option = $("#dateType").val();
            var dVal = $("#datePicker").val();
            switch(option){
                case 'day':
                    $('#value').val(""+dVal+"");
                    break;
                //case 'month':
                //    $("#value").val(""+dVal+"-01");
                //    break;
                //case 'year':
                //    $("#value").val(""+dVal+"-01-01");
                //    break;
                default:
                    $('#value').val(""+dVal+"");
                    break;
            }
            service.getPayMentsByType($('#chooseByDate'),$('#bySum').val(),$('#dateType').val());
            
        });
        
    },
}

var processFunction =  {
    
    locationSuccess : function(currentLocation){
        $('#lon').val(currentLocation['longitude']);
        $('#lat').val(currentLocation['latitude']);
        $('#amount').val(currentLocation['accuracy']);
        
        service.getGoogleAddress(currentLocation);
        service.getNearBy(currentLocation);
       
    },
    
    locationFailure : function(errorMsg){
        alert(errorMsg);
    }
    
}