var charts = {
            data        : null,
            activeChart : null ,
            dateType    : null,
            drawPieChart : function() {
                        dataArray = charts.data;
                        
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'sumType');
                        data.addColumn('number', 'amount');
                        for (var key in dataArray.data){
                                    if (dataArray.data[key].type)
                                                data.addRows([[dataArray.data[key].type, parseInt(dataArray.data[key].totalAmount)]]);
                                    else if (dataArray.data[key].payment)
                                                data.addRows([[dataArray.data[key].payment, parseInt(dataArray.data[key].totalAmount)]]);
                                    else if (dataArray.data[key].locations)
                                                data.addRows([[dataArray.data[key].locations, parseInt(dataArray.data[key].totalAmount)]]);
                        }
            
                        // Set chart options
                        var options = {'title':'Transactions',
                                        'width':400,
                                        'height':300};
                  
                        charts.activeChart = new google.visualization.PieChart(document.getElementById('chart_div'));
                        charts.activeChart.draw(data, options);
            },
            drawColumnChart : function() {
                        dataArray = charts.data;
                        dateType = charts.dateType;

                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'date');
                        data.addColumn('number', 'Amount');
                        data.addColumn({type:'string',role:'tooltip','p': {'html': true}});
                        if (dataArray.data){
                                    for (var key in dataArray.data){
                                                var date = toolBelt.explode(dataArray.data[key].date,' ');
                                                
                                                if (dateType == "day")
                                                            dateStatment = date[1];
                                                else if (dateType == "month"){
                                                            var month = toolBelt.explode(date[0],'-');
                                                            dateStatment = month[2];
                                                }
                                                else if (dateType == "year"){
                                                            var month = toolBelt.explode(date[0],'-');
                                                            dateStatment = month[1];
                                                }
                                                            
                                                data.addRows([[dateStatment,parseInt(dataArray.data[key].amount),"<div style='direction:rtl'>"+dataArray.data[key].businessName+"-"+dataArray.data[key].description+" <br><b> ("+parseInt(dataArray.data[key].amount)+")</b> </div>"]]);
                                    }
                        }
                
                        var options = {
                          tooltip : {isHtml: true},
                          title : 'Column Chart',
                          vAxis: {title: "Amount",minValue: "0"},
                          hAxis: {title: "Transactions("+(dataArray.data.length)+")",textPosition: 'out',maxTextLines:'3',minTextSpacing:'30'},
                          chartArea: {'width': '100%'},
                          seriesType: "bars",
                          series: {5: {type: "line"}}
                        };
                        
                        charts.activeChart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                        charts.activeChart.draw(data, options);
                
            },
            drawComboChart : function() {
                        dataArray = charts.data;
                        var tableArray = new Array();
                        tableArray[0] = new Array();
                        tableArray[1] = new Array();
                        tableArray[0][0]='';
                        var arrayCounter = 1;
                        
                        for (var key in dataArray.data){
                                    tableArray[0][arrayCounter] = dataArray.data[key].businessName+"::"+dataArray.data[key].description;
                                    tableArray[1][arrayCounter] = parseInt(dataArray.data[key].amount);
                                    arrayCounter++;
                        }

                        var data = new google.visualization.arrayToDataTable(tableArray);
                
                        var options = {
                          title : 'Combo Chart',
                          vAxis: {title: "Amount",minValue: "0"},
                          hAxis: {title: "Transactions("+(tableArray[0].length-1)+")"},
                          chartArea: {'width': '100%', 'height': '80%','left':'0'},
                          seriesType: "bars",
                          series: {5: {type: "line"}}
                        };
                        
                        charts.activeChart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                        charts.activeChart.draw(data, options);
                
            },
            destroyChart : function(){
                        if (charts.activeChart != null){
                                    charts.activeChart.clearChart()
                                    charts.activeChart = null;
                                    charts.data = null;
                        }
            }
}