var toolBelt = {
  
  constant : function(){
    var setTimeForStop = null;
  },
  
  openSelect : function (elementId){
    var element = document.getElementById(elementId);
    var event;
    event = document.createEvent('MouseEvents');
    event.initMouseEvent('mousedown', true, true, window);
    element.dispatchEvent(event);
    
  },
  
  isJson : function(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  },

  explode : function (myString,symbol){
    // first remove extra spaces
    myString = myString.replace(/[\s]+/g, ' ');    // g = replace all instances

    // then split the string
    var myArray = myString.split(symbol);
    
    return myArray;
  },
  
  print_r : function (arr, level){
    var print_red_text = "";
    if(!level) level = 0;
    var level_padding = "";
    for(var j=0; j<level+1; j++) level_padding += "    ";
    if(typeof(arr) == 'object') {
        for(var item in arr) {
            var value = arr[item];
            if(typeof(value) == 'object') {
                print_red_text += level_padding + "'" + item + "' :\n";
                print_red_text += this.print_r(value,level+1);
		} 
            else 
                print_red_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
        }
    } 

    else  print_red_text = "===>"+arr+"<===("+typeof(arr)+")";
    return print_red_text;
  },
  
  checkForStopTyping : function(onSuccess,timer){
    clearTimeout(toolBelt.constant.setTimeForStop);
    toolBelt.constant.setTimeForStop = setTimeout(function (){onSuccess()},timer);
  },
  
  setSelectionRange : function(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(selectionStart, selectionEnd);
    }
    else if (input.createTextRange) {
      var range = input.createTextRange();
      range.collapse(true);
      range.moveEnd('character', selectionEnd);
      range.moveStart('character', selectionStart);
      range.select();
    }
  },

  setCaretToPos : function(input, pos) {
    toolBelt.setSelectionRange(input, pos, pos);
  }
  
};