<?php require_once('header.php'); ?>
<?php

    $form = new form;
        $form->setMethod('POST');
        $form->setAction('search.php');
        $form->setForm($formArray->getSummerByComboChartForm());
        $form->setTableClass('chooseByDate');
        $form->setFormId('chooseByDate');
        $form->setFieldset(true);
        $form->setLegend('Combo Chart');
        echo $form->createForm();
        
?>
<div id="chart_div"></div>