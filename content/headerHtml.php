<?php
$versionNumber = 6;//time();
$_SESSION['preFix'] = ".";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title></title>
    <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=HEBREW">
    <meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=UTF-8" />
    <meta HTTP-EQUIV="Content-language" CONTENT="he">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1, maximum-scale=1,  user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!--<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">-->
    <!--<meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">-->
    <meta NAME="ROBOTS" CONTENT="ALL"> 
    <link rel="stylesheet" href="<?php echo $_SESSION['preFix'] ?>/style/style.css?tmp=<?php echo $versionNumber ?>">
    <script src="<?php echo $_SESSION['preFix'] ?>/js/jquery1.9.js"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/hammer.js"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/hammer_jquery.js"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/storage.js?tmp=<?php echo $versionNumber ?>"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/session.js?tmp=<?php echo $versionNumber ?>"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/location.js?tmp=<?php echo $versionNumber ?>"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/main.js?tmp=<?php echo $versionNumber ?>"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/menu.js?tmp=<?php echo $versionNumber ?>"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/service.js?tmp=<?php echo $versionNumber ?>"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/pageFunction.js?tmp=<?php echo $versionNumber ?>"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/toolBelt.js?tmp=<?php echo $versionNumber ?>"></script>
    <script src="<?php echo $_SESSION['preFix'] ?>/js/charts.js?tmp=<?php echo $versionNumber ?>"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script>service.preFix = "<?php echo $_SESSION['preFix'] ?>";</script>
</head>
<body>
    <div id="loadAnim"><img id="loadGif" src="<?php echo $_SESSION['preFix'] ?>/style/images/loadAnim.gif" /></div>
