<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
$fileName = $_SERVER['SCRIPT_FILENAME'];
if( !strpos($fileName,'index.php')>0){
    if (!isset($_SESSION['userInfo']['token'])):
        header('Location: index.php');
        exit();
    endif;
}

require_once("../class/form/uploadFile.php");

?>
<?php

if (isset($_FILES['uploadFile'])){

    $uploadFile = new uploadFile();
    $allowedExts = array("jpg","jpeg","gif","png","bmp");
    $fileType = array("image/jpeg","image/jpg","image/gif","image/bmp","image/png");
    $fileSize = "15000000";
    $extension = end(explode(".", $_FILES['uploadFile']["name"]));
    $fileName = $_SESSION['userInfo']['userName'].time();
    if(!file_exists("../upload/".$_SESSION['userInfo']['userName'])){
        mkdir("../upload/".$_SESSION['userInfo']['userName'], 0705, true);
    }
    if(!file_exists("../upload/".$_SESSION['userInfo']['userName']."/".date("m_Y"))){
        mkdir("../upload/".$_SESSION['userInfo']['userName']."/".date("m_Y"), 0705, true);
    }
    
    $uploadFolder="../upload/".$_SESSION['userInfo']['userName']."/".date("m_Y");
    
    $uploadFile->setAllowedExts($allowedExts);
    $uploadFile->setFileType($fileType);
    $uploadFile->setFileSize($fileSize);
    $uploadFile->setUploadFolder($uploadFolder);
    $uploadFile->setFileName($fileName);
    $fileSuccess = $uploadFile->upload($_FILES['uploadFile']);
    $fullFileName = $fileName.'.'.$extension;
    if (($fileSuccess == 'File uploaded successfully')&&($_FILES['uploadFile']["size"]/1024>1500)){
        $uploadFile->rotate_image("../upload/".$_SESSION['userInfo']['userName']."/".date("m_Y")."/".$fullFileName,270);
        $uploadFile->compress_image("../upload/".$_SESSION['userInfo']['userName']."/".date("m_Y")."/".$fullFileName, "../upload/".$_SESSION['userInfo']['userName']."/".date("m_Y")."/min_".$fullFileName, 50);
        unlink("../upload/".$_SESSION['userInfo']['userName']."/".date("m_Y")."/".$fullFileName);
        $fullFileName = "min_".$fullFileName;
    }
    $response['url'] = "http://".$_SERVER["SERVER_NAME"]."/upload/".$_SESSION['userInfo']['userName']."/".date("m_Y")."/".$fullFileName;
    $response['server'] = $fileSuccess;
    echo json_encode($response);

}

?>