<?php
    session_start();
    if(isset($_REQUEST['token'])){

        if( (strpos(decrypt($_REQUEST['token'],'public'),'dor')>-1)&&(strpos(decrypt($_REQUEST['token'],'public'),'cohen')>0) ){
            $_SESSION['userInfo']['token'] = encrypt(decrypt($_REQUEST['token'],'public'),'private');
            $_SESSION['userInfo']['firstName'] = $_REQUEST['firstName'];
            $_SESSION['userInfo']['lastName'] = $_REQUEST['lastName'];
            $_SESSION['userInfo']['phonePre'] = $_REQUEST['phonePre'];
            $_SESSION['userInfo']['phonePost'] = $_REQUEST['phonePost'];
            $_SESSION['userInfo']['eMail'] = $_REQUEST['eMail'];
            $_SESSION['userInfo']['userName'] = $_REQUEST['userName'];
            
            echo encrypt(decrypt($_REQUEST['token'],'public'),'private');
            return true;
        }
        else{
            return false;
        }
        exit();
    }
        /*  -------->   Encrypt Function   <--------*/
        function encrypt($data,$keyCup){
            $privkey = 'DorCohenServer123456';
            $pubkey  = 'DorCohenApplicationCode123456';    
            if ($keyCup == 'public')
                $key = md5($pubkey);
            else 
                $key = md5($privkey);
                    
            $plaintext_utf8 = utf8_encode($data);
            
            $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext_utf8, MCRYPT_MODE_ECB);
    
            $ciphertext_base64 = (string)bin2hex($ciphertext);
                    
            return  trim($ciphertext_base64) ;
            
        }
        
        /*  -------->   Decrypt Function   <--------*/
        function decrypt($data,$keyCup){
            $privkey = 'DorCohenServer123456';
            $pubkey  = 'DorCohenApplicationCode123456';
            if ($keyCup == 'public')
                $key = md5($pubkey);
            else 
                $key = md5($privkey);
            
            $ciphertext_base64 = hex2bin($data);
            
            $ciphertext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_base64, MCRYPT_MODE_ECB);
            
            $plaintext_utf8 = (string)utf8_decode($ciphertext);
            
            return  trim($plaintext_utf8) ;
        
        }
        
        /*  -------->   Utils Functions   <--------*/
        function hex2bin($h)
        {
            if ((!is_string($h))||(strlen($h)%2!=0))
                return null;
            $r='';
            for ($a=0; $a<strlen($h); $a+=2) {
                    $r.=chr(hexdec($h{$a}.$h{($a+1)}));
            }
            return $r;
        }
?>