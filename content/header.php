<?php

$fileName = $_SERVER['SCRIPT_FILENAME'];
header('Content-Type: text/html; charset=UTF-8');
session_start();
if((( !strpos($fileName,'login.php')>0)) && (!strpos($fileName,'index.php')>0)){
    if (!isset($_SESSION['userInfo']['token'])){
        require_once('exitWithReload.php');
        exit();
    }
}



require_once("../class/form/inputBox.php");
require_once("../class/form/selectBox.php");
require_once("../class/form/uploadFile.php");
require_once("../class/form/textArea.php");
require_once("../class/form/form.php");
require_once("../class/handlers/setForms.php");
require_once("../class/config/timezone.php");

$formArray = new setForms();
$timeZone = new timeZone();
$timeZone->setTimeZone('Asia/Tel_Aviv');


?>


